#### Checkout the sources

```bash
$ mkdir -p ~/src/git/
$ cd ~/src/git/
$ git clone https://gitlab.com/kistlermirrors/kibox1-build.git
```

#### Configure the build

The following will configure a build directory
`yocto-builds/kibox` in your home directory

```bash
$ cd kibox-build/yocto/scripts/
$ ./setupEnvironment.sh ~/yocto-builds/kibox
  ... lots of output
$ cd ..
$ source poky/oe-init-build-env ~/yocto-builds/kibox
```

Note: The last command will leave you into the
`~/yocto-builds/kibox` directory

#### Run BitBake

Build the skyboard evaluation board image:

```sh
$ bitbake core-image-minimal
   ... lots and lots of output
```

