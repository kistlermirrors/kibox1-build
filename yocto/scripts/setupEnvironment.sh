#! /bin/bash

YOCTO_VERSION=rocko-18.0.0
SDK_DISTRO_VERSION=2.4
SCRIPT_DIR_REL=`dirname "$0"`
TOP_DIR=`readlink -nf ${SCRIPT_DIR_REL}/../../`
YOCTO_BUILD_DIR=${TOP_DIR}/yocto/${YOCTO_VERSION}-build
META_KIBOX_DIR=${TOP_DIR}/yocto/meta-kibox-common

# store env variables in top directory
echo YOCTO_VERSION=$YOCTO_VERSION > "${TOP_DIR}/.env_export"
echo SDK_DISTRO_VERSION=$SDK_DISTRO_VERSION >> "${TOP_DIR}/.env_export"

echo DEBUG:
echo TOP_DIR = $TOP_DIR
echo YOCTO_BUILD_DIR = $YOCTO_BUILD_DIR

# show help in case of wrong usage or if help is requested
function show_help
{
    echo " usage: setupEnvironment.sh [<option>] [<build-dir>]"
    echo ""
    echo " <options>:"
    echo " --help     show this information"
    echo " --cleanup  cleanup yocto tree completely (do this from time to time)"
    echo ""
    echo " <build-dir>:  build directory for all generated stuff (needs lots of disk space)"
    echo "               default: yocto/<release-name>-build"
    echo ""
    echo ""
    echo " System requirements:                   "
    echo " - see README for information on        "
    echo "   requirements on your host disto      "
    echo "----------------------------------------"
    exit
}

# clean up existing directory
function cleanup_yocto
{
    echo "[INFO]: removing previous yocto directories..."
    cd $TOP_DIR
    sudo rm -rf .env_export
    sudo rm -rf ${YOCTO_BUILD_DIR}
    sudo rm -rf yocto/poky-${YOCTO_VERSION}
    echo "[INFO]: directories removed"
}

# create new yocto build directory
function install_yocto
{
    cd ${TOP_DIR}/yocto

    # checkout all submodules
    echo "[INFO]: Checking out submodules... "
    git submodule update --init --checkout --force
    echo ""
    echo "Submodule revisions:"
    git submodule status

    # for backwards compatibility, create yocto/poky-${YOCTO_VERSION}
    rm -rf poky-${YOCTO_VERSION}
    ln -fs poky poky-${YOCTO_VERSION}

    # create build directory
    echo -n "[INFO]: creating build directory..."
    mkdir -p ${YOCTO_BUILD_DIR}
    mkdir -p ${YOCTO_BUILD_DIR}/conf
    echo "done"

    # create local configuration
    cp ${META_KIBOX_DIR}/conf/local.conf.template ${YOCTO_BUILD_DIR}/conf/local.conf
    cp ${META_KIBOX_DIR}/conf/bblayers.conf.template ${YOCTO_BUILD_DIR}/conf/bblayers.conf
    sed -i "s,##OEROOT##,${TOP_DIR},g" ${YOCTO_BUILD_DIR}/conf/bblayers.conf

    # print instructions on how to build
    echo "--------------------------------------------------------------"
    echo "---  BUILD INSTRUCTIONS                                    ---"
    echo "--------------------------------------------------------------"
    echo " Before each build, run the following command to setup your   "
    echo " environment:"
    echo " $> source ${TOP_DIR}/yocto/poky/oe-init-build-env ${YOCTO_BUILD_DIR}"
    echo ""
    echo " (this will jump to build dir) then start the build using:"
    echo " MACHINE=<machine> bitbake <target>"
    echo " with the following <machine> and <target> choices:"
    echo " values for <machine>:"
    echo "    skyboard-evb : Skyboard BSP layer provided by ${META_KISTLER_DIR}"
    echo "    intel-corei7-64 :x86 boards"
    echo "    beaglebone   : beaglebone BSP layer provided by meta-ti"
    echo " values for <target>:"
    echo "    core-image-minimal :      minimal image with just console provided by yocto"
    echo "    core-image-full-cmdline : command line image for x86 boards"
    echo "    core-image-kibeaglebone : image for beaglebone provided by ${META_KISTLER_DIR}"
    echo "    meta-ide-support :        complete cross toolchain"
    echo "--------------------------------------------------------------"
}

# ENTRY POINT ######################################################## <<<----- Script will start here!
echo "----------------------------------------"
echo "$0"
echo " based on:"
echo " Yocto v. $YOCTO_VERSION"
echo -n " SkyBase rev: "
echo `git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`
echo "----------------------------------------"

# parse parameters
if [ $# -gt 2 ]
then
    echo "[ERROR]: wrong number of arguments"
    show_help
fi

if [ "$1" = "--help" ]
then
    show_help
    exit
elif [ "$1" = "--cleanup" ]
then
    if [ $# -eq 2 ]
    then
        YOCTO_BUILD_DIR=$2
    fi
    echo "[INFO]: cleaning up build directory: ${YOCTO_BUILD_DIR}"
    cleanup_yocto
elif [ $# -eq 1 ]
then
    YOCTO_BUILD_DIR=$1
fi

echo "[INFO]: using build directory: ${YOCTO_BUILD_DIR}"
install_yocto
